from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodolistForm, TodoItemForm
def todo_list_list(request):
    todo_list2 = TodoList.objects.all()
    context = {
        "todo_list3": todo_list2
    }
    return render (request, "todos/todo_list4.html", context)

def todo_list_detail(request, id):
    todo_list_detail1 = get_object_or_404(TodoList, id=id)
    tasks = todo_list_detail1.items.all()
    context = {
        "todo_list_detail2": todo_list_detail1,
        "tasks1": tasks
    }
    return render(request, "todos/todo_list_detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form1 = TodolistForm(request.POST)
        if form1.is_valid():
            list1 = form1.save()
            return redirect("todo_list_detail", id=list1.id)
    else:
        form1 = TodolistForm()
    context = {
        "form2": form1,
    }
    return render(request, "todos/todo_list_create.html", context)

def todo_list_update(request, id):
    todo_list7 = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodolistForm(request.POST, instance=todo_list7)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodolistForm(instance=todo_list7)
    context = {
        "todo_list8": todo_list7,
        "post_form": form,
    }
    return render(request, "todos/todo_list_update.html", context)

def todo_list_delete(request, id):
    todo_list9 = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list9.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item1 = form.save()
            todo_item1.save()
            return redirect("todo_list_detail", id=todo_item1.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form3": form,
    }
    return render(request, "todos/todo_item_create.html", context)

def todo_item_update(request, id):
    todo_item3 = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item3)
        if form.is_valid():
            todo_item4 = form.save()
            todo_item4.save()
            return redirect("todo_list_detail", id=todo_item4.list.id)
    else:
        form = TodoItemForm(instance=todo_item3)
    context = {
        "form4": form
    }
    return render(request, "todos/todo_item_update.html", context)
